/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuates.controlador;

import java.io.Serializable;

/**
 *
 * @author LAWL
 */
public class Cuate1 implements Serializable{
    private String Apellidos;
    private String Nombres;
    private double cuotas;
    private final String foto;
    
    public Cuate1(String Apellidos, String Nombres, double cuotas, String foto){
        this.Apellidos=Apellidos;
        this.Nombres=Nombres;
        this.cuotas=cuotas;
        this.foto=foto;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public double getCuotas() {
        return cuotas;
    }

    public void setCuotas(double cuotas) {
        this.cuotas = cuotas;
    }

    public String getFoto() {
        return foto;
    }


    
    
    
}
