package cuates.controlador;

import java.util.Collection;


public interface CuateBuscaInterface
{
    public CuateController BuscaCuate(String user, String password);
    public Collection<CuateController> ListaCuates();
}
