package cuates.controlador;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//@ManagedBean(name="buscaCuate")
//@SessionScoped
public class CuateMapeo implements CuateBuscaInterface
{
    private final Map<String,CuateController> cuates;
    private final Collection<CuateController> lista = new ArrayList<>();
    
    public CuateMapeo()
    {
        cuates = new HashMap<>();
        System.out.println("Creando cuates");
        addCuate(new CuateController("gerard", "lel21", "Ruiz", "Gerardo", 1679.27));
        addCuate(new CuateController("nikki", "dos42", "Alazañez", "Nict", 79.17));
        addCuate(new CuateController("dadi", "asdf", "Diaz", "Daniel", 547.702));
        addCuate(new CuateController("diego", "yes123", "J", "D", 10.10));
    }
    
    @Override
    public CuateController BuscaCuate(String user, String password)
    {
        CuateController cuate=null;
        System.out.println(user);
        if(user!=null)
        {
            cuate = cuates.get(user.toLowerCase());
            if(cuate!=null)
            {
                if(cuate.getPassword().equals(password))
                {
                    System.out.println("Es igual el password");
                    return cuate;
                }
                else
                {
                    return null;
                }
            }
        }
        return cuate;
    }
    
    private void addCuate(CuateController cuate)
    {
        System.out.println("Cuotas: "+ cuate.getCuotas());
        lista.add(cuate);
        cuates.put(cuate.getUser(), cuate);
    }

    @Override
    public Collection<CuateController> ListaCuates()
    {
        return lista;
    }
}
