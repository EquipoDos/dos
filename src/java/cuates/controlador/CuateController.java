package cuates.controlador;

public class CuateController
{
    private String user;
    private String password;
    private String apellidos;
    private String nombres;
    private double cuotas;
    
    public CuateController(String user, String password, String apellidos, String nombres, double cuotas)
    {
        this.user=user;
        this.password=password;
        this.apellidos=apellidos;
        this.nombres=nombres;
        this.cuotas=cuotas;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getApellidos() 
    {
        return apellidos;
    }

    public void setApellidos(String apellidos) 
    {
        this.apellidos = apellidos;
    }

    public String getNombres() 
    {
        return nombres;
    }

    public void setNombres(String nombres)
    {
        this.nombres = nombres;
    }

    public double getCuotas()
    {
        return cuotas;
    }

    public void setCuotas(double cuotas)
    {
        this.cuotas = cuotas;
    }
}
