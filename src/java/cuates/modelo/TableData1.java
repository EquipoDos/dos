/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuates.modelo;

import cuates.controlador.Cuate1;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author LAWL
 */
@ManagedBean
@SessionScoped
public class TableData1 implements Serializable{

    private static final Cuate1[] cuates= new Cuate1[]{
        new Cuate1("Lopez Avila","Juan Antonio",2345.66,"1"),
        new Cuate1("Ortega Viera","Alma",335.66,"2"),
        new Cuate1("Casa Jimenez","Felicia",35.00,"3"),
        new Cuate1("Lopez Gutierrez","Francisco",78.50,"4"),
        
    };
    
    public Cuate1[] getCuates(){
        return cuates;
    }   
    
    
    public TableData1() {
    }
    
}
