package cuates.modelo;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

@ManagedBean(name="configForma")
@SessionScoped
public class configForma
{
    private boolean isEspaniol = true;
    private String lenguaje="es";
    private static final Map<String,String> lenguaje_map = new LinkedHashMap<>();
    
    static
    {
        lenguaje_map.put("English", "en");
        lenguaje_map.put("Español", "es");
    }
    
    public configForma() {
    }
    
    
    public String getLenguaje() {
        return lenguaje;
    }

    
    public Map<String,String> getLenguajes()
    {
        return lenguaje_map;
    }
}
