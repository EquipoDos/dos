package cuates.modelo;

import cuates.controlador.*;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="cuate")
@SessionScoped
public class cuate
{
    private String user;
    private String password;
    private CuateController cuate;
    private Collection<CuateController> listaCuates;
    
//    @ManagedProperty(value="#{buscaCuate}")
    private CuateBuscaInterface cuates = new CuateMapeo();
    //cuates = new CuateMapeo();
    
    public cuate()
    {
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public CuateController getCuate()
    {
        return cuate;
    }

    public void setCuate(CuateController cuate)
    {
        this.cuate = cuate;
    }
    
    public CuateBuscaInterface getCuates()
    {
        return cuates;
    }

    public void setCuates(CuateBuscaInterface cuates)
    {
        this.cuates = cuates;
    }
    
    public String pagoCuotas()
    {
        String pagina;
        cuate = cuates.BuscaCuate(user, password);
        
        if(cuate==null)
        {
            pagina="Intruso";
        }
        else
        {
            if(cuate.getCuotas()<100)
            {
                pagina="Deudor";
            }
            else if(cuate.getNombres().equals("Gerardo"))
            {
                pagina="ListaCuates";
            }
            else
            {
                pagina="ListaCuates";
            }
        }
        return pagina;
    }

    public Collection<CuateController> getListaCuates() {
        listaCuates=cuates.ListaCuates();
        return listaCuates;
    }
}
